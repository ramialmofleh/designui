//
//  ContentView.swift
//  DesignUI
//
//  Created by rami on 17.08.21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        ButtonsView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
