//
//  ButtonsView.swift
//  DesignUI
//
//  Created by rami on 17.08.21.
//

import SwiftUI

struct ButtonsView: View {
    var body: some View {
        ScrollView {
            VStack (spacing: 16) {
                VStack (spacing: 16) {
                    Text("Rect")
                            .font(.title2)
                            .fontWeight(.bold)
                    Button("Primary") {}
                            .buttonStyle(RBtn(capsule: false, color: "primary"))
                }
                        .padding(.bottom, 30)
                
                VStack (spacing: 16) {
                    Text("Rounded")
                            .font(.title2)
                            .fontWeight(.bold)
                    Button("Secondary") {}
                            .buttonStyle(RBtn(color: "secondary"))
                }
                        .padding(.bottom, 30)
                
                VStack (spacing: 16) {
                    Text("Bordered")
                            .font(.title2)
                            .fontWeight(.bold)
                    Button("Danger") {}
                        .buttonStyle(RBtn(capsule: false,color: "danger"))
                        .disabled(true)
                }
                        .padding(.bottom, 30)
            }
                    .padding()
        }
        
    }
}

struct ButtonsView_Previews: PreviewProvider {
    static var previews: some View {
        ButtonsView()
    }
}
