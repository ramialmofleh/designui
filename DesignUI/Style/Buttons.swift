//
//  Buttons.swift
//  DesignUI
//
//  Created by rami on 17.08.21.
//

import SwiftUI

struct RBtn: ButtonStyle {
    @State var capsule: Bool? = true
    @State var color: String? = "primary"
    @State var fullWidth: Bool? = true
    @State var bordered: Bool? = false
    @State var opacity = 1.0
    @State var tapped = false
    
    func makeBody(configuration: Configuration) -> some View {
        if capsule == true {
            if bordered == true {
                configuration.label
                    .frame(maxWidth: .infinity)
                    .padding()
                    .background(bordered! ? Color.clear : Color(color!))
                    .foregroundColor(Color(checkForegroundColor(color: color!, bordered: bordered!)))
                    .overlay(
                                RoundedRectangle(cornerRadius: 25.0)
                                    .stroke(Color(color!), lineWidth: 1)
                            )
                    .opacity(tapped ? 0.5 : 1.0)
                    .gesture(LongPressGesture().onChanged { _ in self.tapped.toggle()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                            self.tapped.toggle()
                        }
                    })
            } else {
                configuration.label
                    .frame(maxWidth: .infinity)
                    .padding()
                    .background(bordered! ? Color.clear : Color(color!))
                    .foregroundColor(Color(checkForegroundColor(color: color!, bordered: bordered!)))
                    .clipShape(RoundedRectangle(cornerRadius: 25.0, style: .continuous))
                    .opacity(tapped ? 0.5 : 1.0)
                    .gesture(LongPressGesture().onChanged { _ in self.tapped.toggle()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                            self.tapped.toggle()
                        }
                    })
                
            }
        } else {
            configuration.label
                .frame(maxWidth: .infinity)
                .padding()
                .background(bordered! ? Color.clear : Color(color!))
                .foregroundColor(Color(checkForegroundColor(color: color!, bordered: bordered!)))
                .border(Color(color!), width: bordered == true ? 1 : 0)
                .opacity(tapped ? 0.5 : 1.0)
                .gesture(LongPressGesture().onChanged { _ in self.tapped.toggle()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                        self.tapped.toggle()
                    }
                })
        }
    }
        
    
    private func checkForegroundColor(color: String, bordered: Bool) -> String {
        if color == "dark" ||
            color == "primary" ||
            color == "secondary" ||
            color == "success" ||
            color == "info" ||
            color == "dark" {
            return "light"
        } else if color == "light" || color == "warning" {
            return "dark"
        } else if color == "danger" {
            if bordered {
                return "danger"
            } else {
                return "light"
            }
        } else {
            return "dark"
        }
    }
}

