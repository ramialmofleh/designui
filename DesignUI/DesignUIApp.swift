//
//  DesignUIApp.swift
//  DesignUI
//
//  Created by rami on 17.08.21.
//

import SwiftUI

@main
struct DesignUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
